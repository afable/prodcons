# prodcons
Simple producer/consumer.

-------------------------------------
Description:
-------------------------------------
A threaded producer/consumer program using a circular array and semaphores.
Producers sleep either 1 or 4 seconds after each production. Consumers sleep either
1 or 5 seconds after each consumption.

Creator: Erik Afable
Created: October 3, 2011
Last updated: January 4, 2012

Scaffolding of code taken from the following examples:
http://cs.gmu.edu/~white/CS571/Examples/Pthread/create.c (skeleton for creating pthread routines)
http://www.cs.cmu.edu/afs/cs.cmu.edu/academic/class/15213-s03/www/thread_examples.pdf,
Problem 1: Program 3 (understanding pthread_exit return values)

