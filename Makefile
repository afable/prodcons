prodcons: prodcons.o
	gcc -g -Wall -Werror prodcons.o -lpthread -pthread -o prodcons

prodcons.o: prodcons.c
	gcc -c -g -Wall -Werror prodcons.c

clean:
	rm -f *.o
	rm -f prodcons

