/* File: prodcons.c
 * Description: A threaded producer/consumer program using a circular array and semaphores.
 *		Producers sleep either 1 or 4 seconds after each production. Consumers sleep either
 *		1 or 5 seconds after each consumption.
 * Creator: Erik Afable
 * Created: October 3, 2011
 * Last updated: January 4, 2012
 *
 * Scaffolding of code taken from the following examples:
 *		http://cs.gmu.edu/~white/CS571/Examples/Pthread/create.c (skeleton for creating pthread routines)
 *		http://www.cs.cmu.edu/afs/cs.cmu.edu/academic/class/15213-s03/www/thread_examples.pdf,
 *			Problem 1: Program 3 (understanding pthread_exit return values)
 */

/* Preprocessor directives */
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <semaphore.h>

#define BUFFER_SIZE 32
#define MAX_LOOP 100
#define PRODUCE_RANGE 100
#define FATIGUED 2
#define ONE_SEC 1
#define FOUR_SEC 4
#define FIVE_SEC 5

/* Function prototypes */
static int buffer_init();
static void *producer_routine();
static void *consumer_routine();

/* Global variables */
static int *start;
static int *end;
static int buffer[BUFFER_SIZE]; 
static sem_t empty;
static sem_t occupied;

/* Description: Initializes buffer to 0 values and sets start, end to point to
 * 		the first element of buffer.
 * Params: None.
 * Returns: int 0 on success.
 */
int buffer_init()
{
	memset(buffer, 0, BUFFER_SIZE);
	start = &buffer[0];
	end = &buffer[0];
	return 0;
}

/* Description: A producer routine that runs on its own thread. The producer
 * 		adds int values [0, 99] to the buffer circular array, decrements the
 *		empty semaphore and start pointer, and increments the occupied semaphore.
 *		If the buffer is full, a print msg is sent to stdout.
 * Params: None.
 * Returns: Void.
 */
void *producer_routine()
{
	int i;
	int empty_value = 0;
	unsigned int producer = 19851161;

	for (i = 0; i < MAX_LOOP; ++i)
	{
		/* Retrieve and test if the empty semaphore is less than 0. Positive 
		 * values indicate an empty spot is available to add and we decrement empty.
		 * If empty is 0, we print "buffer full, waiting...\n" and sem_wait will
		 * block until empty is incremented by the consumer_routine.
		 */
		sem_getvalue(&empty, &empty_value);
		if ( empty_value <= 0 )
		{
			fputs( "buffer full, waiting...\n", stderr );
			sem_wait(&empty);
		}
		else
			sem_wait(&empty);

		//sleep(1); // test if consumer_routine feels producer_routine's bottleneck
		
		/* Generate a random number from 0 to PRODUCE_RANGE and add that number
		 * to the circular array buffer. Print produced value to stdout.
		 */
		*start = rand_r(&producer) % PRODUCE_RANGE;
		printf("produced %d\n", *start);
		if ( start != &buffer[BUFFER_SIZE-1] )
			++start;
		else
			start = &buffer[0];

		/* Increment occupied semaphore. This allows consumer_routine to
		 * consume a value if it is sem_waiting on occupied.
		 */
		sem_post(&occupied);
		
		/* If the producer is FATIGUED, sleep for 4 seconds.
		 * Otherwise sleep for 1 second. There is a 50% chance of either case.
		 */
		 if ( rand_r(&producer) % FATIGUED )
		 	 sleep(FOUR_SEC);
		 else
		 	 sleep(ONE_SEC);
	}
	pthread_exit((void **)0);
}

/* Description: A consumer routine that runs on its own thread. The consumer
 * 		retrieves values from the buffer circular array, increments the empty
 *		semaphore and end pointer, and decrements the occupied semaphore. If
 *		the buffer is empty, a print message is sent to stdout.
 * Params: None.
 * Returns: Void.
 */
void *consumer_routine()
{
	int j;
	int value;
	unsigned int consumer = 19881002;
	int occupied_value = 0;

	for (j = 0; j < MAX_LOOP; ++j)
	{
		/* Retrieve and test if the occupied semaphore is less than 0. Positive 
		 * values indicate an occupied spot is available to consume and we decrement
		 * occupied. If occupied is 0, we print "buffer empty, waiting...\n" and
		 * sem_wait will block until occupied is incremented by the producer_routine.
		 */
		sem_getvalue(&occupied, &occupied_value);
		if ( occupied_value <= 0 )
		{
			fputs( "buffer empty, waiting...\n", stderr );
			sem_wait(&occupied);
		}
		else
			sem_wait(&occupied);
		
		//sleep(1); // test if producer_routine feels consumer_routine's bottleneck
		
		value = *end;
		printf("consumed %d\n", value);
		if ( end < &buffer[BUFFER_SIZE-1] )
			++end;
		else
			end = &buffer[0];

		/* Increment empty semaphore. This allows producer_routine to add a value
		 * if it is sem_waiting on empty.
		 */
		sem_post(&empty);

		/* If the consumer is FATIGUED, sleep for 5 seconds.
		 * Otherwise sleep for 1 second. There is a 50% chance of either case.
		 */
		if ( rand_r(&consumer) % FATIGUED )
			sleep(FIVE_SEC);
		else
			sleep(ONE_SEC);
	}
	pthread_exit((void**)0);
}

/* Description: Creates producer and consumer pthreads. Errors for creating and
 * 		exiting the pthreads are handled.
 * Params: None.
 * Returns: int 0 on success.
 */
int main(int argc, char *argv[])
{
	pthread_t prod_t, cons_t;
	int p_status, c_status;
	long int error;
	
	/* Initialize buffer and empty, occupied semaphores (semaphores must be initialized). */
	buffer_init();
	/* Empty starts with value BUFFER_SIZE, i.e. BUFFER_SIZE spaces are available
	 * for the producer before it blocks on sem_wait(&empty) */
	sem_init(&empty, 0, BUFFER_SIZE);
	/* Occupied starts with value 0, i.e. NO spaces are initially available for the consumer,
	 * so it starts blocked on sem_wait(&occupied) */
	sem_init(&occupied, 0, 0);

	error = pthread_create(&prod_t, NULL, producer_routine, NULL);
	if ( error )
		printf("Error in producer_routine pthread_create: %ld\n", error);

	error = pthread_create(&cons_t, NULL, consumer_routine, NULL);
	if ( error )
		printf("Error in consumer_routine pthread_create: %ld\n", error);

	pthread_join(prod_t, (void **)&p_status);
	pthread_join(cons_t, (void **)&c_status);
	
	if ( p_status )
		fprintf(stderr, "Error in producer_routine pthread_exit: %i\n", p_status);
	
	if ( c_status )
		fprintf(stderr, "Error in producer_routine pthread_exit: %i\n", c_status);

	pthread_exit((void **)0);
}



